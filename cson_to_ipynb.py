import os
import sys
import re
import json
from jinja2 import Environment, BaseLoader
from argparse import ArgumentParser, RawTextHelpFormatter, SUPPRESS


def md_to_ipynb(md):
    data = {}
    data['cells'] = []
    data['cells'].append({
        'cell_type': 'markdown',
        # 'execution_count': None,
        'metadata': {},
        # 'outputs': [],
        'source': ['\n' if x == '' else x for x in md]
    })

    data['metadata'] = {}
    data['metadata']['kernelspec'] = {}
    data['metadata']['kernelspec']['display_name'] = "Python 3"
    data['metadata']['kernelspec']['language'] = "python"
    data['metadata']['kernelspec']['name'] = "python3"

    data['metadata']['language_info'] = {}
    data['metadata']['language_info']['codemirror_mode'] = {}
    data['metadata']['language_info']['codemirror_mode']['name'] = 'ipython'
    data['metadata']['language_info']['codemirror_mode']['version'] = 3

    data['metadata']['language_info']['file_extension'] = ".py"
    data['metadata']['language_info']['mimetype'] = "text/x-python"
    data['metadata']['language_info']['name'] = "python"
    data['metadata']['language_info']['nbconvert_exporter'] = "python"
    data['metadata']['language_info']['pygments_lexer'] = "ipython3"
    data['metadata']['language_info']['version'] = "3.7.5"

    data['nbformat'] = 4
    data['nbformat_minor'] = 4

    result = json.dumps(data, indent=2)

    return result


def cson_to_md(cson):
    folder_id = None
    title = None
    cson_content_list = open(cson, 'r', encoding='utf-8').readlines()
    # cson_content = '\n'.join(cson_content_list)

    md_content_no_top = []
    grab_content = False
    for cson_line in cson_content_list:
        if grab_content:
            md_content_no_top.append(cson_line)  # .lstrip())

        if cson_line.strip().startswith("folder:"):
            regex_folder = re.compile(r'folder:\s*"(.+)"')
            folder_id = regex_folder.findall(cson_line.strip())

        if cson_line.strip().startswith("title:"):
            regex_title = re.compile(r'title:\s*"(.+)"')
            title = regex_title.findall(cson_line.strip())

        if cson_line.strip() == "content: '''":
            grab_content = True

    md_content = []
    grab_content = False
    for cson_line in reversed(md_content_no_top):
        if grab_content:
            md_content.append(cson_line)

        if cson_line.strip() == "'''":
            grab_content = True

    md_content.reverse()

    title = title[0] if title else None
    folder_id = folder_id[0] if folder_id else None

    return md_content, title, folder_id


def get_folders_names(boostnote_json):
    boostnote_json = open(boostnote_json, 'r', encoding='utf-8').read()

    folders = json.loads(boostnote_json)['folders']

    folders_name_id_dict = {}
    folders_id_name_dict = {}
    for json_element in folders:
        folder_name = json_element['name']
        folder_id = json_element['key']

        folders_name_id_dict[folder_name] = folder_id
        folders_id_name_dict[folder_id] = folder_name

    return folders_name_id_dict, folders_id_name_dict


def fix_filename(filename):
    new_char = '-'
    return filename.replace('/', new_char).replace('<', new_char).replace('>', new_char).replace('?', new_char).replace(';', new_char).replace(':', new_char).replace('@', new_char).replace('!', new_char).replace('*', new_char)


def check_args():
    parser = ArgumentParser(formatter_class=RawTextHelpFormatter, description="""
Converter of cson files to markdown and ipynb formats
""")

    # Options
    parser.add_argument("-debug", dest="debug", action='store_true', help="if True, leaves the latex conversion files",
                        default=False)

    parser.add_argument("-s", "--single-folder", dest='single_folder', action='store_true',
                        help="if True, stores all files in the single output directory", default=False)

    parser.add_argument("input_root", help="Input root to iterate and search for files to convert")

    parser.add_argument("-o", "--output-root", dest="output_root", help="Output folder", required=True)

    parser.add_argument("-f", "--output-format", dest="output_format", help="Output format", default='ipynb',
                        choices=['md', 'ipynb'])

    args = parser.parse_args()  # it returns input as variables (args.dest)

    # end check args

    return args


def main(args=None):
    if args is None:
        args = check_args()

    input_root_fullpath = os.path.abspath(args.input_root)

    # get folders' names from folders' ids
    foldername_folderid__dict, folderid_foldername__dict = get_folders_names(os.path.join(os.path.dirname(input_root_fullpath), 'boostnote.json'))

    for root, folders, files in os.walk(input_root_fullpath):
        files = [x for x in files if os.path.splitext(x)[1].lower() == '.cson']
        fullpath_files = [os.path.join(root, x) for x in files if os.path.splitext(x)[1].lower() == '.cson']

        for fpf in fullpath_files:
            if input_root_fullpath != root:
                fpf_rel = os.path.relpath(root, input_root_fullpath)
            else:
                fpf_rel = None

            md_content, md_title, md_folder_id = cson_to_md(fpf)

            if not md_content or not md_title:
                continue

            if args.output_format == 'ipynb':
                ipynb_content = md_to_ipynb(md_content)

                file_content = ipynb_content
            else:  # md
                file_content = ''.join(md_content)

            # write output converted file
            # output_filename = os.path.splitext(os.path.basename(fpf))[0] + '.' + args.output_format
            output_filename = fix_filename(md_title) + '.' + args.output_format

            if not args.single_folder:  # tree
                if fpf_rel:
                    fpf_out = os.path.join(args.output_root, fpf_rel, folderid_foldername__dict[md_folder_id], output_filename)
                else:
                    fpf_out = os.path.join(args.output_root, folderid_foldername__dict[md_folder_id], output_filename)

            else:
                if fpf_rel:
                    fpf_out = os.path.join(args.output_root, fpf_rel, output_filename)
                else:
                    fpf_out = os.path.join(args.output_root, output_filename)

            # create output folder if missing
            output_folder = os.path.dirname(fpf_out)
            if not os.path.exists(output_folder):
                os.makedirs(output_folder)

            open(fpf_out, 'w', encoding='utf-8').writelines(file_content)


if __name__ == '__main__':
    try:
        main(args=None)
    except KeyboardInterrupt:
        print('\n\nBye!')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)

#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = "Dario Necco"

import locale
import os
import sys
import argparse
from argparse import ArgumentParser, RawTextHelpFormatter
import logging
import inspect
import json
import time
import shutil
import io
import re
import subprocess
from subprocess import Popen, PIPE
from sys import platform as _platform
import traceback
import pprint
from distutils import dir_util
from xml.dom import minidom
import datetime
# import chardet  # uncomment only if necessary, it requires installation

# import Namespace
try:
    from types import SimpleNamespace as Namespace  # available from Python 3.3
except ImportError:
    class Namespace:
        def __init__(self, **kwargs):
            self.__dict__.update(kwargs)

if sys.version[0] == '2':
    from imp import reload

    reload(sys)
    sys.setdefaultencoding("utf-8")

# import Colorama
try:
    from colorama import init, Fore, Back, Style
    init(strip=True)  # strip makes colorama working in PyCharm
except ImportError:
    print('Colorama not imported')

locale.setlocale(locale.LC_ALL, 'C')  # set locale

# set version and author  
__version__ = 1.0

# I obtain the app directory
if getattr(sys, 'frozen', False):
    # frozen
    dirapp = os.path.dirname(sys.executable)
    dirapp_bundle = sys._MEIPASS
    executable_name = os.path.basename(sys.executable)
else:
    # unfrozen
    dirapp = os.path.dirname(os.path.realpath(__file__))
    dirapp_bundle = dirapp
    executable_name = os.path.basename(__file__)

##############################################################################################
# DEBUG
this_scriptFile = inspect.getfile(inspect.currentframe())
this_scriptFile_filename = os.path.basename(this_scriptFile)
this_scriptFile_filename_noext, ext = os.path.splitext(this_scriptFile_filename)

# logging.basicConfig(filename=this_scriptFile_filename_noext + '.log', level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')  # uncomment for Logging

welcome_text = '{char1} {appname} v.{version} {char2}'.format(char1='-' * 5,
                                                              appname=os.path.splitext(os.path.basename(__file__))[0], 
                                                              version=__version__, char2='-' * 5)
print(welcome_text)
logging.info(welcome_text)


def print_color(text, color):
    print('{color}{text}{reset_color}'.format(color=color, text=text, reset_color=Style.RESET_ALL))


def print_error(text):
    print('{color}{text}{reset_color}'.format(color=Fore.RED, text=text, reset_color=Style.RESET_ALL))


def print_warning(text):
    print('{color}{text}{reset_color}'.format(color=Fore.YELLOW, text=text, reset_color=Style.RESET_ALL))


def check_args():
    parser = ArgumentParser(formatter_class=RawTextHelpFormatter, description="""
    Description

    """)

    # Options
    parser.add_argument("-debug", dest="debug", action='store_true', help=argparse.SUPPRESS)
                        # help='Increase output verbosity. Output files don\\\\'t overwrite original ones'

    parser.add_argument("-op", "--output", dest="output_root", default=os.path.dirname(__file__),
                        help="output markdown root")
                        # action='store_true'
                        # nargs="+",
                        # nargs='?',  # optional argument
                        # default=""
                        # type=int
                        # choices=[]

    parser.add_argument("root",
                        help="root of tree to convert")

    args = parser.parse_args()  # it returns input as variables (args.dest)

    # end check args

    return args


def main(args=None):
    if args is None:
        args = check_args()

    # Recusive search
    for dirpath, dirnames, filenames in os.walk(args.root):
        for filename in [f for f in filenames]:  # for all files in subdirs
            # print os.path.join(dirpath, filename)

            if not filename.lower().endswith('.ipynb'):
                continue

            file = os.path.join(dirpath, filename)
            file_md_content = ''
            with open(file, 'r', encoding='utf-8', errors='ignore') as read_ipynb:
                ipynb_dict = json.loads(read_ipynb.read())

            for cell in ipynb_dict['cells']:
                file_md_content += ''.join(cell['source'])

            # save content to md file
            md_file = os.path.splitext(file)[0] + '.md'
            file_rel_path = os.path.relpath(md_file, args.root)
            output_fullpath = os.path.join(args.output_root, file_rel_path)

            output_dirpath = os.path.dirname(output_fullpath)
            if not os.path.exists(output_dirpath):
                os.makedirs(output_dirpath)

            with open(output_fullpath, 'w', encoding='utf-8', errors='ignore') as write_md:
                write_md.write(file_md_content)


if __name__ == '__main__':
    try:
        main(args=None)
    except KeyboardInterrupt:
        print('\\\\n\\\\nBye!')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
